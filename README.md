# Gitpitch trial

This is a repo to try out [Git Pitch](https://gitpitch.com/).

Originally tried using [Codenvy](https://codenvy.com/) connected to [ScrumIT](http://www.scrumit.co.uk/txp/) repo on Gitlab.

Now using P9 remote IDE as that seems to be much more functional (I could generate key pairs on it.)

This is to capture my thoughts on what I can deliver to Gitlab in the role of global recruiter.

I'm not applying for any technical positions, my involvement in gitlab projects will come for free.



