#HSLIDE

#### Mel Pullen for Gitlab Global Recruiter

Mel will be the best recruiter for Gitlab because he understands:
- Gitlab
- People
- Remote working
- Agile development practices
- Antifragility

#VSLIDE?image=./titch1.jpg

#### <span style="color:#ffffff">Work how I want</span>

<span style="color:#ff0a0a">I use a standing desk. I use the [Pomodoro](https://play.google.com/store/apps/details?id=com.libtronics.tomatina&hl=en_GB) technique; one of the cats will come and pester me for attention on a regular basis.</span>

<span style="color:#d80e0e">I sometimes work early, or late at night. Sometimes I go and sit on a train or a bus for isolation. If I'm stuck I go for a cycle ride to blow the cobwebs out of my head.</span>


#HSLIDE

#### Possibly cosmopolitan
I was born in Australia and have lived in England, Jersey, Belgium, France, Germany, Switzerland and USA mostly for work. I have visited Holland, Denmark, Sweden, Finland, Greece, India and China for work. I have learnt English, French, German, a little Spanish and Greek. I am learning Portuguese. 

#HSLIDE

#### Multi-cultural
In my current job I deal with people learning Agile development practices in UK, Holland, Poland and India. I have to ensure that language of written documents has no cultural mystery.

#HSLIDE

#### Assessing the ability & the fit
I have worked in a UK university and created an examination paper for non-native English speakers. It contained programs that the student needed to parse.

I heard the two attributes for a candidate are:
- Good
- Gets things done

#HSLIDE

#### I can do what it takes

An agent told me what his life is like:
- "Imagine making 150 calls by lunch time"

#HSLIDE

#### Biog
- Experienced developer, first program in 1968
- Freelance programmer for 25 years
- Lived and worked in many countries
- Worked in product and user environment
- Last four years in Enterprise
- Scrum Master since 2003
- Trained developers in team working
- Worked off-site in many jobs since 1980
- Home office since 2008
- Great communicator, speaker, writer

#HSLIDE

#### Why not apply for something else?

I believe this position will provide best return on investment to help grow Gitlab. There are many programmers who would like to work for Gitlab, who will be pleased to use a recently learned skill. I'm no longer a 'jobbing' coder.

I should have retired a couple of years ago. I did retire to start my own business, and that taught me I can't run a business.

My recent experience confirms that _planned_ solutions rarely work, so that's also a learning experience.

#VSLIDE

Gitlab gets all these features for free:
- Architect - I love diagrams
- Experience of culture change
- I'll start projects and programmes
- I'll get back to coding mobile apps

#HSLIDE

#### Drive by discussions
Radio Couloir - discussions outside of specific projects. Provides serendipitious discovery based on common problems.

So extend conversational development outside the team or project.

#HSLIDE

#### Extend conversations outside projects

Allow people 'wandering past' in the 'corridor' to get an 'augenblick' of the state of a project.

- "show me the kanban board"
- "show me the burn down chart"
- "show me cost of delay of this idea"
- "show me the backlog ordered by cost of delay"
- "show me the unexplored backlog items"


#HSLIDE

#### Create the "gitlab empire"

Extending conversations outside of projects creates a community of interest far larger than the existing staff of gitlab and provides a candidate pool for future recruitment.

Allows for peer review before candidates are identified as such.

A comment from [this blog post about conversational development](https://blog.planrockr.com/about-conversational-development-b93ad4a1df3b#.xosos7ubq):
 "The rest of the organization can contribute"

#HSLIDE

### Develop the Gitlab Recruitment Practice

#### Develop candidate tools

#### Develop recruiter tools

#### Develop customer tools

#VSLIDE

#### Develop candidate tools

Recruitment needs a web present. What better way to make it collaborative? Use a web creation tool like [Gatsby](<https://github.com/gatsbyjs/gatsby>)

There are many assessment tools that can be used to assess the personality of the people applying. As I believe that Gitlab should create a diaspora that is far larger than the direct staff there should be people who are known to the organisation.

#VSLIDE
#### Develop recruiter tools

One of the hardest things to do is to decide what qualities, skills, experience are needed and what can be acquired by the right candidate.

From this will come training, either by selecting the best of breed such as [react training](https://reactforbeginners.com/) or by creating it internally.

#VSLIDE

#### Develop customer tools

As Gitlab gains more customers there will be requests to provide resources as well as the services. There will be opportunities for capability assessments and coaching.


#HSLIDE?image=./DuneView1.JPG

#### <span style="color:#d80e0e">Working remotely</span>

<span style="color:#d80e0e">I live across the road from the beach. Why work in an office?</span>

<span style="color:#d80e0e">Thinking of moving to Portugal, hence learning the language.</span>

